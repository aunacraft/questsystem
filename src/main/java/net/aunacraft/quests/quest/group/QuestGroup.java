package net.aunacraft.quests.quest.group;

import com.google.common.collect.Lists;
import lombok.Getter;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.Quest;
import org.bukkit.Material;

import java.util.List;

@Getter
public abstract class QuestGroup {

    private final String identifier;
    private final String nameKey;
    private final List<Quest> quests = Lists.newArrayList();
    private final Material iconMaterial;
    private final QuestGroupType type;

    public QuestGroup(String identifier, String nameKey, Material iconMaterial, QuestGroupType type) {
        this.identifier = identifier;
        this.nameKey = nameKey;
        this.iconMaterial = iconMaterial;
        this.type = type;
        this.initQuests(this.quests);
        this.quests.forEach(quest -> quest.setGroup(this));
    }

    public abstract void initQuests(List<Quest> quests);

    public Quest getNextQuest(QuestPlayer player, Quest finishedQuest) {
        int index = this.quests.indexOf(finishedQuest) + 1;
        if(index > quests.size() - 1) {
            this.handleFinish(player);
            return null;
        }
        return quests.get(index);
    }

    public Quest getFirstQuest() {
        if(this.quests.size() == 0) return null;
        return this.quests.get(0);
    }

    public Quest getLastQuest() {
        return this.quests.get(quests.size() - 1);
    }

    public abstract void handleFinish(QuestPlayer player);

    public Quest getQuest(String identifier) {
        return this.quests.stream().filter(quest -> quest.getIdentifier().equals(identifier)).findAny().orElse(null);
    }

}
