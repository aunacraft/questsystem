package net.aunacraft.quests.quest.group;

public enum QuestGroupType {

    CITYBUILD_QUEST, SIDE_QUEST, STORY_QUEST

}
