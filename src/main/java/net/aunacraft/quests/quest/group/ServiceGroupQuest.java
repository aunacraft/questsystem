package net.aunacraft.quests.quest.group;

import lombok.Getter;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.cloud.bridgeclient.network.player.NetworkPlayer;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.event.QuestEvent;

public abstract class ServiceGroupQuest extends Quest {

    @Getter
    private final String serviceGroupName;

    public ServiceGroupQuest(String identifier, String nameKey, String serviceGroupName, int goal, Class<? extends QuestEvent>... events) {
        super(identifier, nameKey, goal, events);
        this.serviceGroupName = serviceGroupName;
    }

    @Override
    public boolean isValidEvent(QuestPlayer player, QuestEvent event) {
        String localServiceGroupName = AunaCloudAPI.getServiceAPI().getLocalService().getGroup().getGroupName();
        if(!localServiceGroupName.equals(serviceGroupName))
            return false;
        return super.isValidEvent(player, event);
    }
}
