package net.aunacraft.quests.quest;

import lombok.Getter;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.event.QuestEvent;
import net.aunacraft.quests.quest.group.QuestGroup;

@Getter
public abstract class Quest {

    private final String identifier;
    private final String nameKey;
    private final int goal;
    private QuestGroup group;
    private final Class<? extends QuestEvent>[] events;

    public Quest(String identifier, String nameKey, int goal, Class<? extends QuestEvent>... events) {
        this.identifier = identifier;
        this.nameKey = nameKey;
        this.goal = goal;
        this.events = events;
    }

    public void setGroup(QuestGroup group) {
        if(this.group != null)
            throw new IllegalStateException("QuestGroup already set");
        this.group = group;
    }

    public boolean isValidEvent(QuestPlayer player, QuestEvent event) {
        return true;
    }

    public final void fireEvent(QuestPlayer player, QuestEvent event) {
        if(isValidEvent(player, event)) {
            player.getProgress(this).addProgressivePoint();
            player.getBossBarHandler().sendBossBar(player.getSelectedQuest());
        }
    }

    public abstract void handleFinish(QuestPlayer player);

    public abstract void handleStart(QuestPlayer player);
}
