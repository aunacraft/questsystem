package net.aunacraft.quests.quest;

import com.google.common.collect.Lists;
import net.aunacraft.quests.quest.group.QuestGroup;
import net.aunacraft.quests.quest.group.QuestGroupType;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class QuestCache {

    private static final CopyOnWriteArrayList<QuestGroup> QUEST_GROUPS = Lists.newCopyOnWriteArrayList();

    public static Quest getQuestByIdentifier(String identifier) {
        return getAllQuests().stream().filter(quest -> quest.getIdentifier().equals(identifier)).findAny().orElse(null);
    }

    public static List<Quest> getAllQuests() {
        List<Quest> quests = Lists.newArrayList();
        QUEST_GROUPS.forEach(questGroup -> quests.addAll(questGroup.getQuests()));
        return quests;
    }

    public static void addGroup(QuestGroup group) {
        QUEST_GROUPS.add(group);
    }

    public static QuestGroup getGroup(String identifier) {
        return QUEST_GROUPS.stream().filter(questGroup -> questGroup.getIdentifier().equalsIgnoreCase(identifier)).findAny().orElse(null);
    }

    public static QuestGroup getGroup(Class<? extends QuestGroup> groupClass) {
        return QUEST_GROUPS.stream().filter(questGroup -> questGroup.getClass().equals(groupClass)).findAny().orElse(null);
    }

    public static CopyOnWriteArrayList<QuestGroup> getQuestGroups(QuestGroupType type) {
        CopyOnWriteArrayList<QuestGroup> GROUPS = Lists.newCopyOnWriteArrayList();
        QUEST_GROUPS.stream().filter(group -> group.getType() == type).forEach(GROUPS::add);
        return GROUPS;
    }

    public static CopyOnWriteArrayList<QuestGroup> getQuestGroups() {
        return QUEST_GROUPS;
    }
}
