package net.aunacraft.quests.quest.impl.tutorial;

import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.event.impl.MoveBlockQuestEvent;

public class Walk10MeterQuest extends Quest {

    public Walk10MeterQuest() {
        super("tutorial_walk_ten_meters", "questsystem.quests.tutorial.1", 10, MoveBlockQuestEvent.class);
    }

    @Override
    public void handleFinish(QuestPlayer player) {
        player.getPlayer().sendRawMessage("Quest 1 beendet");
    }

    @Override
    public void handleStart(QuestPlayer player) {

    }
}
