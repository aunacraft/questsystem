package net.aunacraft.quests.quest.impl;

import net.aunacraft.quests.player.QuestPlayer;
import org.bukkit.boss.BarColor;

public interface CustomBossBar {

    double getProgress(QuestPlayer player);

    String getBossBarTitleEnding(QuestPlayer player);

    BarColor getCustomBarColor(QuestPlayer player);
}
