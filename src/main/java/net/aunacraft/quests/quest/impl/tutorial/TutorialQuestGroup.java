package net.aunacraft.quests.quest.impl.tutorial;

import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.group.QuestGroup;
import net.aunacraft.quests.quest.group.QuestGroupType;
import org.bukkit.Material;

import java.util.List;

public class TutorialQuestGroup extends QuestGroup {

    public TutorialQuestGroup() {
        super("test", "questsystem.quests.tutorial", Material.MAP, QuestGroupType.STORY_QUEST);
    }

    @Override
    public void initQuests(List<Quest> quests) {
        quests.add(new Walk10MeterQuest());
        quests.add(new Walk100MeterQuest());
    }

    @Override
    public void handleFinish(QuestPlayer player) {
        player.enableQuestGroup(TestQuestGroup.class);
    }
}
