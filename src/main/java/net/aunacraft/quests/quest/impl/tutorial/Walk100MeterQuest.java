package net.aunacraft.quests.quest.impl.tutorial;

import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.event.impl.MoveBlockQuestEvent;

public class Walk100MeterQuest extends Quest {

    public Walk100MeterQuest() {
        super("tutorial_walk_100_meters", "questsystem.quests.tutorial.2", 100, MoveBlockQuestEvent.class);
    }

    @Override
    public void handleFinish(QuestPlayer player) {
        player.getPlayer().sendRawMessage("Quest 2 beendet");
    }

    @Override
    public void handleStart(QuestPlayer player) {

    }
}
