package net.aunacraft.quests.quest.impl.tutorial;

import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.group.QuestGroup;
import net.aunacraft.quests.quest.group.QuestGroupType;
import net.aunacraft.quests.quest.impl.GoToLocationQuest;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;

import java.util.List;

public class TestQuestGroup extends QuestGroup {

    public TestQuestGroup() {
        super("tutorial", "questsystem.quests.test", Material.BEACON, QuestGroupType.SIDE_QUEST);
    }

    @Override
    public void initQuests(List<Quest> quests) {
        quests.add(new Walk10MeterQuest());
        quests.add(new Walk100MeterQuest());
        quests.add(new GoToLocationQuest(new Location(Bukkit.getWorld("world"), 200D, 72D, -320D), "questsystem.quests.tutorial.3", 3, "tutorial_quest_3", "Lobby"));
    }

    @Override
    public void handleFinish(QuestPlayer player) {

    }
}
