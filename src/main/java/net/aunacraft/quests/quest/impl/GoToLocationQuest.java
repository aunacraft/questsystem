package net.aunacraft.quests.quest.impl;

import com.google.common.collect.Lists;
import net.aunacraft.api.util.MCMath;
import net.aunacraft.cloud.api.AunaCloudAPI;
import net.aunacraft.quests.QuestSystem;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.event.QuestEvent;
import net.aunacraft.quests.quest.event.impl.MoveQuestEvent;
import net.aunacraft.quests.quest.tickable.TickAbleServiceGroupQuest;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.boss.BarColor;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.util.Vector;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class GoToLocationQuest extends TickAbleServiceGroupQuest implements CustomBossBar {

    private final Location centerLocation;
    private final List<Location> circleLocations;
    private final double radius;
    private final String positionNameKey;

    public GoToLocationQuest(Location centerLocation, String positionNameKey, double radius, String uniqueIdentifier, String serviceGroupName) {
        super("go_to_location_" + uniqueIdentifier, "questsystem.quests.go_to_location", serviceGroupName, 1, 5, MoveQuestEvent.class);
        this.centerLocation = centerLocation;
        this.radius = radius;
        this.positionNameKey = positionNameKey;
        this.circleLocations = new MCMath().getCircleLocations(this.centerLocation, this.radius, 150);
    }


    //show particles
    @Override
    public void tick(QuestPlayer questPlayer) {
        Player player = questPlayer.getPlayer().toBukkitPlayer();

        Location playerLocation = player.getLocation();
        if(!isValidWorldAndService(questPlayer)) {
            questPlayer.getBossBarHandler().sendBossBar(questPlayer.getSelectedQuest());
            return;
        }
        double distance = playerLocation.distance(this.centerLocation);

        double space = 0.3;

        //draw line
        Vector p1 = playerLocation.clone().add(0, 1.3, 0).toVector();
        Vector p2 = centerLocation.toVector();
        Vector vector = p2.clone().subtract(p1).normalize().multiply(space);

        List<Location> locations = Lists.newArrayList();
        for (int i = 0; i < 5 / space && i < distance; i++) {
            locations.add(p1.toLocation(playerLocation.getWorld()));
            p1.add(vector);
        }
        new Animation(player, locations);

        //draw circle
        if(distance < 150) {
            for (Location circleLocation : circleLocations) {
                player.spawnParticle(Particle.DRIP_LAVA, circleLocation, 1, 0D, 0D, 0D);
            }
        }
        questPlayer.getBossBarHandler().sendBossBar(questPlayer.getSelectedQuest());
    }

    @Override
    public void handleFinish(QuestPlayer player) {
        player.getPlayer().sendMessage("questsystem.quests.go_to_location.finish", player.getPlayer().getMessage(this.positionNameKey));
    }

    @Override
    public void handleStart(QuestPlayer player) {
        player.getPlayer().sendMessage("questsystem.quests.go_to_location.start", player.getPlayer().getMessage(this.positionNameKey));
    }

    private boolean isValidWorldAndService(QuestPlayer questPlayer) {
        Player player = questPlayer.getPlayer().toBukkitPlayer();
        if(!Objects.equals(player.getLocation().getWorld(), this.centerLocation.getWorld())) return false;
        if(!AunaCloudAPI.getServiceAPI().getLocalService().getGroup().getGroupName().equals(this.getServiceGroupName())) return false;
        return true;
    }

    @Override
    public boolean isValidEvent(QuestPlayer player, QuestEvent event) {
        if(event instanceof MoveQuestEvent) {
            PlayerMoveEvent moveEvent = ((MoveQuestEvent)event).getEvent();
            Location playerLocation = moveEvent.getPlayer().getLocation();
            if(!isValidWorldAndService(player)) return false;
            double distance = playerLocation.distance(this.centerLocation);
            if(distance <= radius) return true;
        }
        return false;
    }

    @Override
    public double getProgress(QuestPlayer player) {
        Location playerLocation = player.getPlayer().toBukkitPlayer().getLocation();
        if(!isValidWorldAndService(player)) return 1;
        double distance = playerLocation.distance(this.centerLocation);
        if(distance <= 500) {
            return 1 - (distance / 500);
        }
        return 0;
    }

    @Override
    public String getBossBarTitleEnding(QuestPlayer player) {
        Location playerLocation = player.getPlayer().toBukkitPlayer().getLocation();
        if(!isValidWorldAndService(player)) return " §8| " + player.getPlayer().getMessage("questsystem.quests.go_to_location.invalid_world");
        double distance = playerLocation.distance(this.centerLocation);
        return "§8(§7" + Math.round(distance) + "m§8)";
    }

    @Override
    public BarColor getCustomBarColor(QuestPlayer player) {
        if(!isValidWorldAndService(player)) return BarColor.YELLOW;
        return null;
    }

    private class Animation {

        private int taskID;

        public Animation(Player player, List<Location> locations) {
            AtomicInteger step = new AtomicInteger(0);
            this.taskID = Bukkit.getScheduler().scheduleSyncRepeatingTask(QuestSystem.getPlugin(QuestSystem.class), () -> {
                if(step.get() >= locations.size() || !player.isOnline()) {
                    Bukkit.getScheduler().cancelTask(this.taskID);
                    return;
                }

                for(int i = 0; i <= step.get(); i++) {
                    player.spawnParticle(Particle.REDSTONE, locations.get(i), 10, new Particle.DustOptions(getColor(i), 0.35F));
                }
                step.getAndAdd(5);
            }, 0, 1);
        }

        private Color getColor(int i) {
            if(i > 9) return getColor(i - 9);
            return switch (i) {
                case 0 -> Color.RED;
                case 1 -> Color.ORANGE;
                case 2 -> Color.YELLOW;
                case 3 -> Color.LIME;
                case 4 -> Color.GREEN;
                case 5 -> Color.OLIVE;
                case 6 -> Color.BLUE;
                case 7 -> Color.NAVY;
                case 8 -> Color.AQUA;
                default -> Color.PURPLE;
            };
        }
    }
}
