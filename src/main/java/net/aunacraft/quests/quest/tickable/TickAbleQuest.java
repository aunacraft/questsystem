package net.aunacraft.quests.quest.tickable;

import net.aunacraft.quests.player.QuestPlayer;

public interface TickAbleQuest {

    void tick(QuestPlayer player);

    int getTickDelay();
}
