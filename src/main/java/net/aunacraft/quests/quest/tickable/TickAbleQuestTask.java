package net.aunacraft.quests.quest.tickable;

import com.google.common.collect.Maps;
import net.aunacraft.quests.QuestSystem;
import net.aunacraft.quests.player.QuestPlayerCache;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.QuestCache;
import org.bukkit.Bukkit;

import java.util.HashMap;

public class TickAbleQuestTask implements Runnable {

    private final HashMap<TickAbleQuest, Long> lastTicks = Maps.newHashMap();

    public TickAbleQuestTask() {
        Bukkit.getScheduler().scheduleSyncRepeatingTask(QuestSystem.getPlugin(QuestSystem.class), this, 1, 1);
    }

    @Override
    public void run() {
        QuestCache.getAllQuests().forEach(quest -> {
            if(quest instanceof TickAbleQuest tickAbleQuest) {
                if(lastTicks.containsKey(tickAbleQuest)) {
                    long lastTick = lastTicks.get(tickAbleQuest);
                    if(lastTick + tickAbleQuest.getTickDelay() * 50L < System.currentTimeMillis()) {
                        this.tick(tickAbleQuest);
                    }
                }else {
                    this.tick(tickAbleQuest);
                }
            }
        });
    }

    private void tick(TickAbleQuest tickAbleQuest) {
        if(tickAbleQuest instanceof Quest) {
            QuestPlayerCache.getPlayersWithSelectedQuest((Quest) tickAbleQuest).forEach(tickAbleQuest::tick);
            lastTicks.put(tickAbleQuest, System.currentTimeMillis());
        }
    }
}
