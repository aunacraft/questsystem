package net.aunacraft.quests.quest.tickable;

import net.aunacraft.quests.quest.event.QuestEvent;
import net.aunacraft.quests.quest.group.ServiceGroupQuest;

public abstract class TickAbleServiceGroupQuest extends ServiceGroupQuest implements TickAbleQuest {

    private final int tickDelay;

    public TickAbleServiceGroupQuest(String identifier, String nameKey, String serviceGroupName, int goal, int tickDelay, Class<? extends QuestEvent>... events) {
        super(identifier, nameKey, serviceGroupName, goal, events);
        this.tickDelay = tickDelay;
    }

    @Override
    public int getTickDelay() {
        return tickDelay;
    }
}

