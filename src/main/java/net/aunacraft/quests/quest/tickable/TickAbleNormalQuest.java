package net.aunacraft.quests.quest.tickable;

import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.event.QuestEvent;

public abstract class TickAbleNormalQuest extends Quest implements TickAbleQuest {

    private final int tickDelay;

    public TickAbleNormalQuest(String identifier, String nameKey, int goal, int tickDelay, Class<? extends QuestEvent>... events) {
        super(identifier, nameKey, goal, events);
        this.tickDelay = tickDelay;
    }

    @Override
    public int getTickDelay() {
        return tickDelay;
    }
}

