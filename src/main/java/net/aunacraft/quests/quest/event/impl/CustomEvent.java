package net.aunacraft.quests.quest.event.impl;

import com.google.common.collect.Maps;
import lombok.Getter;
import net.aunacraft.quests.quest.event.QuestEvent;

import java.util.HashMap;

@Getter
public class CustomEvent extends QuestEvent {

    private final HashMap<String, Object> parameters = Maps.newHashMap();

}
