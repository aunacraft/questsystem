package net.aunacraft.quests.quest.event.impl;

import lombok.Getter;
import net.aunacraft.quests.quest.event.QuestEvent;
import org.bukkit.Location;
import org.bukkit.event.player.PlayerMoveEvent;

@Getter
public class MoveQuestEvent extends QuestEvent {

    private final PlayerMoveEvent event;

    public MoveQuestEvent(PlayerMoveEvent event) {
        this.event = event;
    }
}
