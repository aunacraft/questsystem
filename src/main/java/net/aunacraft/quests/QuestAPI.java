package net.aunacraft.quests;

import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.player.QuestPlayerCache;
import net.aunacraft.quests.quest.QuestCache;
import net.aunacraft.quests.quest.event.QuestEvent;
import net.aunacraft.quests.quest.group.QuestGroup;

import java.util.stream.Collectors;

public class QuestAPI {

    public static void registerQuestGroup(QuestGroup questGroup) {
        QuestCache.addGroup(questGroup);
    }

    public static void callQuestEvent(AunaPlayer player, QuestEvent event) {
        QuestPlayer questPlayer = QuestPlayerCache.getQuestPlayer(player.getUuid());
        if (questPlayer == null) return;
        QuestCache.getAllQuests().stream().filter(quest -> {
            for (Class<? extends QuestEvent> eventClass : quest.getEvents()) {
                if(event.getClass().equals(eventClass)) {
                    return true;
                }
            }
            return false;
        }).collect(Collectors.toList()).forEach(quest -> {
            if(questPlayer.isActiveQuest(quest))
                quest.fireEvent(questPlayer, event);
        });
    }

    public static QuestGroup getGroup(String identifier) {
        return QuestCache.getGroup(identifier);
    }

    public static QuestGroup getGroup(Class<? extends QuestGroup> groupClass) {
        return QuestCache.getGroup(groupClass);
    }
}
