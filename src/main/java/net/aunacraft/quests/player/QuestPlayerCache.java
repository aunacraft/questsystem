package net.aunacraft.quests.player;

import com.google.common.collect.Lists;
import net.aunacraft.quests.quest.Quest;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

public class QuestPlayerCache {

    private static final CopyOnWriteArrayList<QuestPlayer> PLAYERS = Lists.newCopyOnWriteArrayList();

    public static void addPlayer(QuestPlayer player) {
        PLAYERS.add(player);
    }

    public static void removePlayer(QuestPlayer player) {
        PLAYERS.remove(player);
    }

    public static QuestPlayer getQuestPlayer(UUID uuid) {
        return PLAYERS.stream().filter(player -> player.getPlayer().getUuid().equals(uuid)).findAny().orElse(null);
    }

    public static void safeAll() {
        PLAYERS.forEach(QuestPlayer::update);
    }

    public static List<QuestPlayer> getPlayersWithSelectedQuest(Quest quest) {
        return PLAYERS.stream().filter(player -> player.getSelectedQuest() != null && player.getSelectedQuest().equals(quest)).collect(Collectors.toList());
    }
}
