package net.aunacraft.quests.player;

import net.aunacraft.quests.QuestSystem;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.impl.CustomBossBar;
import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarStyle;
import org.bukkit.boss.BossBar;

public class BossBarHandler {

    public final static BarColor DEFAULT_BAR_COLOR = BarColor.BLUE;

    private int taskID;
    private boolean running;
    private final QuestPlayer player;
    private final BossBar bossBar = Bukkit.createBossBar("Quests", BarColor.BLUE, BarStyle.SOLID);

    public BossBarHandler(QuestPlayer player) {
        this.player = player;
    }

    public void sendBossBar(Quest quest) {
        if(running) {
            Bukkit.getScheduler().cancelTask(taskID);
        }
        setBossBarOverlay(quest);
        if(player.getSelectedQuest() != null && !quest.equals(player.getSelectedQuest())) {
            running = true;
            this.taskID = Bukkit.getScheduler().runTaskLater(QuestSystem.getPlugin(QuestSystem.class), () -> {
                if(player.getPlayer().toBukkitPlayer().isOnline() && player.getSelectedQuest() != null)
                    setBossBarOverlay(player.getSelectedQuest());
            }, 20 * 5).getTaskId();
        }
    }

    private void setBossBarOverlay(Quest quest) {
        String ending = "";
        if((quest instanceof CustomBossBar customBossBar))
            ending = customBossBar.getBossBarTitleEnding(this.player);
        String title;
        if(quest == null) {
            title = "§bQuest §8| §c" + player.getPlayer().getMessage("questsystem.quests.nothing");
            bossBar.setProgress(1);
            bossBar.setColor(BarColor.RED);
        }else {
            BarColor color = getBarColor(quest);
            if(!bossBar.getColor().equals(color))
                bossBar.setColor(color);
            title = "§bQuest §8| §3" + player.getPlayer().getMessage(quest.getGroup().getNameKey()) + ": §7" + player.getPlayer().getMessage(quest.getNameKey()) + " " + ending;
            bossBar.setProgress(getProgress(quest));
        }
        if(!bossBar.getTitle().equals(title))
            bossBar.setTitle(title);
        if(!bossBar.getPlayers().contains(player.getPlayer().toBukkitPlayer()))
        bossBar.addPlayer(player.getPlayer().toBukkitPlayer());
    }

    private double getProgress(Quest quest) {
        if(quest instanceof CustomBossBar) return ((CustomBossBar)quest).getProgress(this.player);
        QuestProgress progress = player.getProgress(quest);
        return (double) progress.getProgress() / quest.getGoal();
    }

    private BarColor getBarColor(Quest quest) {
        BarColor color = null;
        if(quest instanceof CustomBossBar)
            color = ((CustomBossBar)quest).getCustomBarColor(player);
        if(color == null)
            color = DEFAULT_BAR_COLOR;
        return color;
    }
}
