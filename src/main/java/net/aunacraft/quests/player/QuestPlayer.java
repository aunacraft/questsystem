package net.aunacraft.quests.player;

import com.google.common.collect.Lists;
import lombok.Getter;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.quests.QuestSystem;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.QuestCache;
import net.aunacraft.quests.quest.group.QuestGroup;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Getter
public class QuestPlayer {

    private final AunaPlayer player;
    private List<QuestProgress> questProgresses = Lists.newArrayList();
    private List<QuestGroup> enabledQuestGroups = Lists.newArrayList();
    private boolean loaded = false;
    private final BossBarHandler bossBarHandler = new BossBarHandler(this);
    private Quest selectedQuest;

    public QuestPlayer(AunaPlayer player) {
        this.player = player;

        QuestSystem.getPlugin(QuestSystem.class).getDatabaseManager().loadPlayerData(this, playerData -> {
            this.questProgresses = playerData.questProgresses();
            this.selectedQuest = playerData.selectedQuest();
            this.enabledQuestGroups = playerData.enabledQuestGroups();

            loaded = true;
            this.bossBarHandler.sendBossBar(this.selectedQuest);

            this.questProgresses.forEach(questProgress -> questProgress.setFinishCallback(() -> this.handleQuestFinish(questProgress)));
        });


    }

    private void handleQuestFinish(QuestProgress progress) {
        Quest quest = progress.getQuest();
        quest.handleFinish(this);
        Quest nextQuest = quest.getGroup().getNextQuest(this, quest);
        this.selectedQuest = null;
        if(nextQuest != null) {
            setSelectedQuest(nextQuest);
            this.questProgresses.add(getProgress(quest));
        }
        this.bossBarHandler.sendBossBar(this.selectedQuest);
        this.update();
    }

    public void enableQuestGroup(Class<? extends QuestGroup> questGroupClass) {
        QuestGroup group = QuestCache.getGroup(questGroupClass);
        if(group == null)
            throw new NullPointerException("Group " + questGroupClass.getName() + " is not registered!");
        this.enabledQuestGroups.add(group);
        this.update();
    }

    public void update() {
        QuestSystem.getPlugin(QuestSystem.class).getDatabaseManager().updatePlayerData(this);
    }

    public QuestProgress getProgress(Quest quest) {
        QuestProgress questProgress = this.questProgresses.stream().filter(progress -> progress.getQuest().equals(quest)).findAny().orElse(null);

        if(questProgress != null)
            return questProgress;
        questProgress = new QuestProgress(quest, 0);
        QuestProgress finalQuestProgress = questProgress;
        questProgress.setFinishCallback(() -> this.handleQuestFinish(finalQuestProgress));
        this.questProgresses.add(questProgress);
        this.update();
        return questProgress;
    }

    public boolean hasProgress(Quest quest) {
        return this.questProgresses.stream().filter(progress -> progress.getQuest().equals(quest)).findAny().orElse(null) != null;
    }

    public String getProgressivesAsJson() {
        List<String> progressivesAsStrings = Lists.newArrayList();
        this.questProgresses.forEach(activeQuests -> progressivesAsStrings.add(activeQuests.toString()));
        return  QuestSystem.GSON.toJson(progressivesAsStrings);
    }

    public String getEnabledQuestGroupsAsJson() {
        List<String> groupsAsStrings = Lists.newArrayList();
        this.enabledQuestGroups.forEach(group -> {
            groupsAsStrings.add(group.getIdentifier());
        });
        return QuestSystem.GSON.toJson(groupsAsStrings);
    }

    public boolean isActiveQuest(Quest quest) {

        //parralel quests
//        QuestProgress progress = this.questProgresses.stream().filter(p -> p.getQuest().equals(quest)).findAny().orElse(null);
//        if(progress == null)
//            return false;
//        return !progress.isFinished();
        return this.selectedQuest != null && this.selectedQuest.equals(quest);
    }

    public boolean hasFinishedGroup(QuestGroup group) {
        if(!this.enabledQuestGroups.contains(group)) return false;
        Quest lastQuest = group.getLastQuest();
        if(this.hasProgress(lastQuest)) {
            QuestProgress progress = this.getProgress(lastQuest);
            return progress.isFinished();
        }
        return false;
    }

    public int getFinishedQuestsAmountOfGroup(QuestGroup group) {
        AtomicInteger finished = new AtomicInteger(0);
        if(hasFinishedGroup(group)) {
            return group.getQuests().size();
        }
        group.getQuests().forEach(quest -> {
            if(hasProgress(quest) && getProgress(quest).isFinished()) {
                finished.addAndGet(1);
            }
        });
        return finished.get();
    }

    public boolean isEnabledQuestGroup(QuestGroup group) {
        return this.enabledQuestGroups.contains(group);
    }

    public void selectGroup(QuestGroup group) {
        for (Quest quest : group.getQuests()) {
            if(!hasProgress(quest)) {
                this.setSelectedQuest(quest);
                break;
            }else if(!getProgress(quest).isFinished()) {
                setSelectedQuest(quest);
                break;
            }
        }
    }

    private void setSelectedQuest(Quest selectedQuest) {
        this.selectedQuest = selectedQuest;
        this.bossBarHandler.sendBossBar(this.selectedQuest);
        selectedQuest.handleStart(this);
    }
}
