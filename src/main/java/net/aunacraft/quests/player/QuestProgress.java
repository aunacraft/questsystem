package net.aunacraft.quests.player;

import com.google.common.collect.Maps;
import com.google.common.reflect.TypeToken;
import lombok.Getter;
import net.aunacraft.api.util.validator.AunaValidators;
import net.aunacraft.quests.QuestSystem;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.QuestCache;
import net.aunacraft.quests.quest.group.QuestGroup;

import java.lang.reflect.Type;
import java.util.HashMap;

public class QuestProgress {

    private static final Type jsonType = new TypeToken<HashMap<String, String>>(){}.getType();

    @Getter private final Quest quest;
    @Getter private int progress;
    private Runnable finishCallback;

    public QuestProgress(Quest quest, int progress) {
        this.quest = quest;
        if(quest == null)
            throw new NullPointerException("Quest is null");
        this.progress = progress;
    }

    public void addProgressivePoint() {
        if(progress + 1 > quest.getGoal()) return;
        this.progress++;
        if(quest.getGoal() <= this.progress && this.finishCallback != null) {
            this.finishCallback.run();
        }
    }

    public void setFinishCallback(Runnable finishCallback) {
        this.finishCallback = finishCallback;
    }

    public String toString() {
        HashMap<String, String> jsonAsMap = Maps.newHashMap();
        jsonAsMap.put("quest", quest.getIdentifier());
        jsonAsMap.put("progress", String.valueOf(this.progress));
        jsonAsMap.put("group", quest.getGroup().getIdentifier());
        return QuestSystem.GSON.toJson(jsonAsMap, jsonType);
    }

    public static QuestProgress getFromString(String json) {
        HashMap<String, String> jsonAsMap = QuestSystem.GSON.fromJson(json, jsonType);
        String questIdentifier = jsonAsMap.get("quest");
        String group = jsonAsMap.get("group");
        if(group == null) return null;
        int progress;
        if(AunaValidators.INT_VALIDATOR.isValid(jsonAsMap.get("progress")))
            progress = Integer.parseInt(jsonAsMap.get("progress"));
        else
            progress = 0;
        QuestGroup questGroup = QuestCache.getGroup(group);
        if(questGroup == null) return null;
        Quest quest = questGroup.getQuest(questIdentifier);
        if(quest == null) return null;
        return new QuestProgress(quest, progress);
    }

    public boolean isFinished() {
        return this.progress >= this.quest.getGoal();
    }
}
