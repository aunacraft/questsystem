package net.aunacraft.quests;

import com.google.gson.Gson;
import lombok.Getter;
import net.aunacraft.api.AunaAPI;
import net.aunacraft.quests.command.QuestCommand;
import net.aunacraft.quests.database.QuestDatabaseManager;
import net.aunacraft.quests.listener.ConnectionListener;
import net.aunacraft.quests.listener.QuestListener;
import net.aunacraft.quests.player.QuestPlayerCache;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.tickable.TickAbleQuestTask;
import net.aunacraft.quests.quest.impl.tutorial.TestQuestGroup;
import net.aunacraft.quests.quest.impl.tutorial.TutorialQuestGroup;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

@Getter
public class QuestSystem extends JavaPlugin {

    public static final Gson GSON = new Gson();

    private QuestDatabaseManager databaseManager;

    @Override
    public void onEnable() {
        this.databaseManager = new QuestDatabaseManager(this);

        new ConnectionListener();
        AunaAPI.getApi().registerCommand(new QuestCommand());

        Bukkit.getPluginManager().registerEvents(new QuestListener(), this);

        addDefaultQuests();

        new TickAbleQuestTask();
    }

    @Override
    public void onDisable() {
        QuestPlayerCache.safeAll();
    }

    private void addDefaultQuests() {
        QuestAPI.registerQuestGroup(new TutorialQuestGroup());
        QuestAPI.registerQuestGroup(new TestQuestGroup());
    }

    public Quest getFirstQuest() {
        return QuestAPI.getGroup(TutorialQuestGroup.class).getFirstQuest();
    }
}
