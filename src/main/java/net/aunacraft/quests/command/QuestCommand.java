package net.aunacraft.quests.command;

import net.aunacraft.api.commands.AunaCommand;
import net.aunacraft.api.commands.Command;
import net.aunacraft.api.commands.builder.CommandBuilder;
import net.aunacraft.quests.QuestSystem;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.player.QuestPlayerCache;
import net.aunacraft.quests.gui.QuestMainGui;

import java.io.File;

public class QuestCommand implements AunaCommand {

    @Override
    public Command createCommand() {
        return CommandBuilder.beginCommand("quest")
                .handler((executor, commandContext, args) -> {
                    QuestPlayer player = QuestPlayerCache.getQuestPlayer(executor.getUuid());
                    if(player != null) {
                        new QuestMainGui(player).open(executor.toBukkitPlayer());
                    }
                })
                .subCommand(
                        CommandBuilder.beginCommand("setnpc")
                                .permission("questsystem.setnpc")
                                .handler((aunaPlayer, commandContext, strings) -> {
                                    QuestSystem questSystem = QuestSystem.getPlugin(QuestSystem.class);
                                    File file = new File(questSystem.getDataFolder(), "npc.json");
                                })
                                .build()
                )
                .build();
    }
}