package net.aunacraft.quests.database;

import com.google.common.collect.Lists;
import net.aunacraft.api.database.DatabaseHandler;
import net.aunacraft.api.database.config.impl.SpigotDatabaseConfig;
import net.aunacraft.quests.QuestSystem;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.player.QuestProgress;
import net.aunacraft.quests.quest.Quest;
import net.aunacraft.quests.quest.QuestCache;
import net.aunacraft.quests.quest.group.QuestGroup;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;
import java.util.*;
import java.util.function.Consumer;

public class QuestDatabaseManager {

    private final DatabaseHandler handler;

    public QuestDatabaseManager(JavaPlugin plugin) {
        this.handler = new DatabaseHandler(new SpigotDatabaseConfig(plugin));
        this.createTables();
        createTables();
    }

    public void createTables() {
        this.handler.createBuilder("CREATE TABLE IF NOT EXISTS quests_players(UUID VARCHAR(100) PRIMARY KEY ," +
                " progress TEXT NOT NULL, selected_quest TEXT NOT NULL, enabledQuests TEXT NOT NULL)").updateAsync();
    }

    public void loadPlayerData(QuestPlayer player, Consumer<QuestPlayerData> progressivesCallback) {
        this.handler.createBuilder("SELECT * FROM quests_players WHERE UUID=?")
                .addObjects(player.getPlayer().getUuid().toString())
                .queryAsync(rs -> {
                    try {
                        if(rs.next()) {
                            List<QuestProgress> progressList = Lists.newArrayList();
                            List<QuestGroup> enabledGroups = Lists.newArrayList();

                            String selectedQuestIdentifier = rs.getString("selected_quest");
                            Quest selectedQuest = Objects.equals(selectedQuestIdentifier, "null") ? null : QuestCache.getQuestByIdentifier(selectedQuestIdentifier);
                            
                            List<String> progresses = QuestSystem.GSON.fromJson(rs.getString("progress"), List.class);
                            for (String progress : progresses) {
                                QuestProgress questProgress = QuestProgress.getFromString(progress);
                                if(questProgress != null) {
                                    progressList.add(questProgress);
                                }
                            }

                            List<String> enabledQuestGroups = QuestSystem.GSON.fromJson(rs.getString("enabledQuests"), List.class);
                            for (String enabledQuestGroup : enabledQuestGroups) {
                                QuestGroup group = QuestCache.getGroup(enabledQuestGroup);
                                if(group != null)
                                    enabledGroups.add(group);
                            }
                            
                            QuestPlayerData questPlayerData = new QuestPlayerData(selectedQuest, progressList, enabledGroups);
                            progressivesCallback.accept(questPlayerData);
                            return;
                        }else {
                            QuestGroup firstGroup = QuestSystem.getPlugin(QuestSystem.class).getFirstQuest().getGroup();
                            player.getEnabledQuestGroups().add(firstGroup);
                            player.selectGroup(firstGroup);
                            insertPlayer(player, () -> progressivesCallback.accept(new QuestPlayerData(player.getSelectedQuest(), player.getQuestProgresses(), player.getEnabledQuestGroups())));
                        }
                    }catch (SQLException e) {
                        e.printStackTrace();
                    }
                });
    }

    private void insertPlayer(QuestPlayer player, Runnable callback) {
        String selectedQuestIdentifier = player.getSelectedQuest() == null ? "null" : player.getSelectedQuest().getIdentifier();
        this.handler.createBuilder("INSERT INTO quests_players (UUID, progress, selected_quest, enabledQuests) VALUES (?,?, ?, ?)")
                .addObjects(player.getPlayer().getUuid().toString(), player.getProgressivesAsJson(), selectedQuestIdentifier, player.getEnabledQuestGroupsAsJson())
                .updateAsync(callback);
    }

    public void updatePlayerData(QuestPlayer player) {
        String selectedQuestIdentifier = player.getSelectedQuest() == null ? "null" : player.getSelectedQuest().getIdentifier();
        this.handler.createBuilder("UPDATE quests_players SET UUID=?, progress=?, selected_quest=?, enabledQuests=? WHERE UUID=?")
                .addObjects(player.getPlayer().getUuid().toString(), player.getProgressivesAsJson(), selectedQuestIdentifier, player.getEnabledQuestGroupsAsJson(),
                        player.getPlayer().getUuid().toString())
                .updateAsync();
    }

    public record QuestPlayerData(Quest selectedQuest, List<QuestProgress> questProgresses, List<QuestGroup> enabledQuestGroups) {

    }
}
