package net.aunacraft.quests.listener;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.event.impl.PlayerRegisterEvent;
import net.aunacraft.api.event.impl.PlayerUnregisterEvent;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.player.QuestPlayerCache;

public class ConnectionListener {

    public ConnectionListener() {

        AunaAPI.getApi().registerEventListener(PlayerRegisterEvent.class, event -> {
            QuestPlayer player = new QuestPlayer(event.getPlayer());
            QuestPlayerCache.addPlayer(player);
        });

        AunaAPI.getApi().registerEventListener(PlayerUnregisterEvent.class, event -> {
            QuestPlayer player = QuestPlayerCache.getQuestPlayer(event.getPlayer().getUuid());
            if (player != null) {
                QuestPlayerCache.removePlayer(player);
                player.update();
            }
        });


    }
}
