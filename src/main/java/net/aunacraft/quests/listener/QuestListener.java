package net.aunacraft.quests.listener;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.quests.QuestAPI;
import net.aunacraft.quests.quest.event.impl.MoveBlockQuestEvent;
import net.aunacraft.quests.quest.event.impl.MoveQuestEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

public class QuestListener implements Listener {

    @EventHandler
    public void move(PlayerMoveEvent event) {
        AunaPlayer player = AunaAPI.getApi().getPlayer(event.getPlayer().getUniqueId());
        if(player == null || player.isLoading() || event.getTo() == null) return;

        MoveQuestEvent moveQuestEvent = new MoveQuestEvent(event);
        QuestAPI.callQuestEvent(player, moveQuestEvent);

        if(!event.getFrom().getBlock().equals(event.getTo().getBlock())) {
            QuestAPI.callQuestEvent(player, new MoveBlockQuestEvent());
        }
    }
}
