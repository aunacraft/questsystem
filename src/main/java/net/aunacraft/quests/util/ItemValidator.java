package net.aunacraft.quests.util;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public interface ItemValidator {

    boolean isValid(ItemStack stack);

    public static class MaterialValidator implements ItemValidator {

        private final Material material;

        public MaterialValidator(Material material) {
            this.material = material;
        }

        @Override
        public boolean isValid(ItemStack stack) {
            return material.equals(stack.getType());
        }
    }

}
