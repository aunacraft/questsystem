package net.aunacraft.quests.gui;

import net.aunacraft.api.AunaAPI;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.player.AunaPlayer;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.QuestCache;
import net.aunacraft.quests.quest.group.QuestGroup;
import net.aunacraft.quests.quest.group.QuestGroupType;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.List;

public class QuestMainGui extends Gui {

    private final QuestPlayer player;

    public QuestMainGui(QuestPlayer player) {
        super(player.getPlayer().getMessage("questsystem.gui.groups"), 9 * 3);
        this.player = player;
    }

    @Override
    public void initInventory(Player bukkitPlayer, GuiInventory guiInventory) {
        AunaPlayer player = AunaAPI.getApi().getPlayer(bukkitPlayer.getUniqueId());
        guiInventory.fill(new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c").build());

        guiInventory.setItem(10,
                new ItemBuilder(Material.MAP)
                        .setDisplayName(player.getMessage("questsystem.gui.main.quests.side"))
                        .setLore("§8---------------------------------", player.getMessage("questsystem.gui.main.quests.side.lore"),
                                player.getMessage("questsystem.gui.main.quests.unlocked",
                                        getUnlockedQuests(QuestGroupType.SIDE_QUEST)))
                        .build(),
                event -> openSubGui(QuestGroupType.SIDE_QUEST));

        guiInventory.setItem(13,
                new ItemBuilder(Material.BOOK)
                        .setDisplayName(player.getMessage("questsystem.gui.main.quests.story"))
                        .setLore("§8---------------------------------", player.getMessage("questsystem.gui.main.quests.story.lore"),
                                player.getMessage("questsystem.gui.main.quests.unlocked",
                                        getUnlockedQuests(QuestGroupType.STORY_QUEST)))
                        .build(),
                event -> openSubGui(QuestGroupType.STORY_QUEST));

        guiInventory.setItem(16,
                new ItemBuilder(Material.GOLDEN_PICKAXE)
                        .setDisplayName(player.getMessage("questsystem.gui.main.quests.citybuild"))
                        .setLore("§8---------------------------------", player.getMessage("questsystem.gui.main.quests.citybuild.lore"),
                                player.getMessage("questsystem.gui.main.quests.unlocked",
                                        getUnlockedQuests(QuestGroupType.CITYBUILD_QUEST)))
                        .build(),
                event -> openSubGui(QuestGroupType.CITYBUILD_QUEST));
    }

    private void openSubGui(QuestGroupType type) {
        new QuestGroupsGui(type, this.player, 0).open(this.player.getPlayer().toBukkitPlayer());
    }

    private String getUnlockedQuests(QuestGroupType type) {
        List<QuestGroup> groups = QuestCache.getQuestGroups(type);
        int i = 0;
        for (QuestGroup questGroup : groups) {
            if(player.isEnabledQuestGroup(questGroup))
                i++;
        }
        return "§7" + i + "§8/§7" + groups.size();
    }
}
