package net.aunacraft.quests.gui;

import com.google.common.collect.Lists;
import net.aunacraft.api.gui.Gui;
import net.aunacraft.api.gui.GuiInventory;
import net.aunacraft.api.util.ItemBuilder;
import net.aunacraft.api.util.SkullBuilder;
import net.aunacraft.quests.player.QuestPlayer;
import net.aunacraft.quests.quest.QuestCache;
import net.aunacraft.quests.quest.group.QuestGroup;
import net.aunacraft.quests.quest.group.QuestGroupType;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class QuestGroupsGui extends Gui {

    private final List<QuestGroup> groups = Lists.newArrayList();
    private final QuestPlayer player;
    private final boolean hasNextPage;
    private final int page;
    private QuestGroupType type;

    private final static ItemStack ARROW_RIGHT_STACK = SkullBuilder.getSkull("http://textures.minecraft.net/texture/682ad1b9cb4dd21259c0d75aa315ff389c3cef752be3949338164bac84a96e");
    private final static ItemStack ARROW_LEFT_STACK = SkullBuilder.getSkull("http://textures.minecraft.net/texture/37aee9a75bf0df7897183015cca0b2a7d755c63388ff01752d5f4419fc645");
    private final static ItemStack QUESTION_MARK_STACK = SkullBuilder.getSkull("http://textures.minecraft.net/texture/46ba63344f49dd1c4f5488e926bf3d9e2b29916a6c50d610bb40a5273dc8c82");

    public QuestGroupsGui(QuestGroupType type, QuestPlayer player, int page) {
        super(player.getPlayer().getMessage("questsystem.gui.groups"), 9 * 3);
        this.type = type;
        this.player = player;
        this.page = page;
        int startIndex = page * 9;
        int endIndex = startIndex + 9;
        List<QuestGroup> enabledQuestGroups = player.getEnabledQuestGroups();
        for (int i = startIndex; i < endIndex && i < enabledQuestGroups.size(); i++) {
            if(isValidQuestGroup(enabledQuestGroups.get(i)))
                groups.add(enabledQuestGroups.get(i));
        }

        for (QuestGroup questGroup : QuestCache.getQuestGroups()) {
            if (this.groups.size() >= 9) break;
            if (isValidQuestGroup(questGroup) && !groups.contains(questGroup))
                groups.add(questGroup);
        }

        this.hasNextPage = enabledQuestGroups.size() > (endIndex + 1);
    }

    private boolean isValidQuestGroup(QuestGroup group) {
        return group.getType() == type;
    }

    @Override
    public void initInventory(Player bukkitPlayer, GuiInventory guiInventory) {
        for (int i = 0; i < 9; i++)
            guiInventory.setItem(i, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c").build());
        for (int i = 18; i < 27; i++)
            guiInventory.setItem(i, new ItemBuilder(Material.BLACK_STAINED_GLASS_PANE).setDisplayName("§c").build());

        guiInventory.setItem(18, new ItemBuilder(Material.BARRIER)
                .setDisplayName(player.getPlayer().getMessage("questsystem.gui.group.back"))
                .build(), event -> {
            new QuestMainGui(player).open(bukkitPlayer);
        });

        int startSlot = 9;
        for (int i = 0; i < groups.size(); i++) {
            QuestGroup group = groups.get(i);

            String lore = player.hasFinishedGroup(group) ? player.getPlayer().getMessage("questsystem.gui.group.finished") :
                    player.getPlayer().getMessage("questsystem.gui.group.open");

            ItemStack stack;
            if(player.isEnabledQuestGroup(group)) {
                stack = new ItemBuilder(group.getIconMaterial())
                        .setDisplayName("§3" + player.getPlayer().getMessage(group.getNameKey()))
                        .setLore("§8---------------------------------", player.getPlayer().getMessage("questsystem.gui.group.progress", "§7" + player.getFinishedQuestsAmountOfGroup(group) + "§8/§7" + group.getQuests().size()),
                                lore)
                        .build();
            }else {
                stack = new ItemBuilder(QUESTION_MARK_STACK.clone())
                        .setDisplayName("§c???")
                        .setLore("§8---------------------------------", player.getPlayer().getMessage("questsystem.gui.group.not_enabled"))
                        .build();
            }
            if (player.getSelectedQuest() != null && player.getSelectedQuest().getGroup().equals(group)) {
                stack = new ItemBuilder(stack)
                        .addEnchant(Enchantment.ARROW_DAMAGE, 1)
                        .addItemFlags(ItemFlag.HIDE_ENCHANTS)
                        .build();
            }
            guiInventory.setItem(i + startSlot, stack, event -> {
                if(player.isEnabledQuestGroup(group) && !player.hasFinishedGroup(group) || (player.getSelectedQuest() != null && !player.getSelectedQuest().getGroup().equals(group))) {
                    player.selectGroup(group);
                    bukkitPlayer.closeInventory();
                    bukkitPlayer.playSound(bukkitPlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BELL, 3, 2);
                }else if(!player.isEnabledQuestGroup(group)) {
                    bukkitPlayer.playSound(bukkitPlayer.getLocation(), Sound.BLOCK_NOTE_BLOCK_BASS, 2, 1);
                }
            });

            if (hasNextPage)
                guiInventory.setItem(26, new ItemBuilder(ARROW_RIGHT_STACK.clone()).setDisplayName(player.getPlayer().getMessage("questsystem.gui.group.next_page")).build()
                        , event -> {
                            new QuestGroupsGui(this.type, this.player, this.page + 1).open((Player) event.getWhoClicked());
                        });
            if (this.page > 0)
                guiInventory.setItem(26, new ItemBuilder(ARROW_LEFT_STACK.clone()).setDisplayName(player.getPlayer().getMessage("questsystem.gui.group.next_page")).build()
                        , event -> {
                            new QuestGroupsGui(this.type, this.player, this.page - 1).open((Player) event.getWhoClicked());
                            ;
                        });
        }
    }
}
